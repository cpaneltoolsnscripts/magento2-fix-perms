How to use.
1.  Change into the Magento document root directory.
2.  Run the below command to start the script fix perms in detached screen session.

`screen -dmS magento_fix_perms bash -c 'bash <(curl https://gitlab.com/cpaneltoolsnscripts/magento2-fix-perms/raw/master/magento2_fixperms.sh || wget -O - https://gitlab.com/cpaneltoolsnscripts/magento2-fix-perms/raw/master/magento2_fixperms.sh); exec sh'`

It will output when it started and completed in a text file named magento-fixperms-status.txt from where the script was run. 