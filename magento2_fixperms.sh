#!/bin/sh
## Author: Michael Ramsey
## Objective Fix Magento2 perms from Magento root directory.
## https://gitlab.com/cpaneltoolsnscripts/magento2-fix-perms
## How to use.
# ./magento2_fixperms.sh
# sh magento2_fixperms.sh

#Magento 2 fixperms. run from document root for the magento installation
echo "Started:$(date '+%Y-%m-%d %H:%M:%S')" | tee magento-fixperms-status.txt

#comment the first two if the main fixperms was done already
find . -type f -exec chmod 644 {} \;
find . -type d -exec chmod 755 {} \;
find ./var -type d -exec chmod 777 {} \;
find ./pub/media -type d -exec chmod 777 {} \;
find ./pub/static -type d -exec chmod 777 {} \;
chmod 777 ./app/etc;
chmod 644 ./app/etc/*.xml;
echo "Completed:$(date '+%Y-%m-%d %H:%M:%S')" | tee -a magento-fixperms-status.txt